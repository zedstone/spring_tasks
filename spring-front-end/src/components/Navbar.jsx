import {AppBar, Avatar, Box, Button, IconButton, Toolbar, Typography} from "@mui/material";
import {styled} from "@mui/material/styles";
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import LoginIcon from '@mui/icons-material/Login';
import PersonSearchIcon from '@mui/icons-material/PersonSearch';


const StyledToolbar = styled(Toolbar)({
    display: "flex",
    justifyContent: "space-between"
});

const StyledIcons = styled(Box)(({ theme}) => ({
    display: "none",
    justifyContent: "space-between",
    width: "100px",
    [theme.breakpoints.up("sm")] : {
        display:"flex",
    }

}));
const StyledAvatar = styled(Box)(({ theme}) => ({
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.up("sm")] : {
        display:"none",
    }

}));

function Navbar() {

    return (
        <AppBar position="sticky">
            <StyledToolbar>
                <Typography sx={{display: {xs:"none", sm:"block"}}}>BankingSpring</Typography>
                <AccountBalanceIcon sx={{display: {xs:"block", sm:"none"}}}></AccountBalanceIcon>
                <StyledIcons> <LoginIcon></LoginIcon>  <PersonSearchIcon></PersonSearchIcon> <Avatar sx={{width : 27, height : 27}} alt="Remy Sharp" src="/static/images/avatar/1.jpg" /></StyledIcons>
                <StyledAvatar><Avatar sx={{width : 27, height : 27}} alt="Remy Sharp" src="/static/images/avatar/1.jpg" /></StyledAvatar>
            </StyledToolbar>

        </AppBar>
    );
}

export default Navbar;
