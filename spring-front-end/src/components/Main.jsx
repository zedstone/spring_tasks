import {Box} from "@mui/material";
import { Routes, Route, Link } from "react-router-dom";
import Customers from "./Customers";
import Rightbar from "./Rightbar";
import User from "./User";
import Accounts from "./Accounts";
import Account from "./Account";



function Main() {


    return (

       <Routes>
            <Route path="/" element={<Rightbar/>}/>
            <Route path="/customers" element={<Customers/>}/>
           <Route path="/customers/:id" element={<User/>}/>
           <Route path="/accounts" element={<Accounts/>}/>
           <Route path="/accounts/:id" element={<Account/>}/>

       </Routes>

    );
}

export default Main;
