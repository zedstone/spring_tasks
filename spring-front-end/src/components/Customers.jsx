import {Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import {useEffect, useState} from "react";
import axios from "axios";
import DeleteIcon from '@mui/icons-material/Delete';
import {Link, useNavigate} from "react-router-dom";
import MouseIcon from '@mui/icons-material/Mouse';


function Customers() {
    const [customers, setCustomers] = useState([]);
    const [tempRefresh, setTempRefresh] = useState(0)
    let navigate = useNavigate();

    useEffect(()=>{
        refreshUser();

    }, [tempRefresh]);

    function refreshUser() {
        axios.get("http://localhost:9000/customers").then(response => {
            setTempRefresh(tempRefresh+1);
            setCustomers(response.data);
        }).catch(error => {
            console.log(error);
        })
    }

    function deleteCustomer(id) {
        console.log("delete" + id);

        axios.delete("http://localhost:9000/customers/" + id).then(response => {

        }).catch(error => {
            console.log(error);
        })
        refreshUser();
        //setCustomers(customers.filter(c=>c.id !== id));
    }


    return (


        <Box bgcolor={"lightcoral"} flex={3} p={2}>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 400 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell >Id</TableCell>
                            <TableCell align="right">Name</TableCell>
                            <TableCell align="right">Email&nbsp;</TableCell>
                            <TableCell align="right">Age&nbsp;</TableCell>
                            <TableCell align="right">Accounts&nbsp;</TableCell>
                            <TableCell align="right">&nbsp;</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {customers.map((data, index) => (
                            <TableRow
                                key={data.name}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {data.id}<Link to={"/customers/" + data.id}> <MouseIcon/></Link>
                                </TableCell>

                                <TableCell align="right">{data.name}</TableCell>
                                <TableCell align="right">{data.email}</TableCell>
                                <TableCell align="right">{data.age}</TableCell>
                                <TableCell align="right">{data.accounts}</TableCell>

                                <TableCell align="right"><DeleteIcon cursor="pointer" onClick={() => deleteCustomer(data.id)}/></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>


        </Box>

    );
}

export default Customers;
