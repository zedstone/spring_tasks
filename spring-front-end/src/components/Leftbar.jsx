import {Box, Collapse, List, ListItem, ListItemButton, ListItemIcon, ListItemText} from "@mui/material";
import {ExpandLess, ExpandMore, Home, StarBorder} from "@mui/icons-material";
import React, {useState} from "react";
import PeopleOutlineIcon from '@mui/icons-material/PeopleOutline';
import AddCardIcon from '@mui/icons-material/AddCard';
import {Link} from "react-router-dom";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import axios from "axios";

function SendIcon() {
    return null;
}

function DraftsIcon() {
    return null;
}

function InboxIcon() {
    return null;
}

function Leftbar() {

    const [open, setOpen] = useState(true);
    const [open2, setOpen2] = useState(true);


    const [inputs, setInputs] = useState({
        name: "",
        email: "",
        age: "",
    });

    const [openModal, setOpenModal] = useState(false);
    const handleOpen = () => setOpenModal(true);
    const handleClose = () => setOpenModal(false);

    const handleClick = () => {
        setOpen(!open);
    };
    const handleClick2 = () => {
        setOpen2(!open2);
    };

    const saveUser = (e) =>{
        e.preventDefault();
        axios.post('http://localhost:9000/customers', {
            name: inputs.name,
            email: inputs.email,
            age: inputs.age
        })
            .then(function (response) {

            })
            .catch(function (error) {
                console.log(error);
            });


        setInputs({
            name: "",
            email: "",
            age: "",
        })
        handleClose();
    }
    const handleChange = (e) => {
        setInputs((prevState) =>({
            ...prevState,
            [e.target.name] : e.target.value,
            [e.target.email] : e.target.email,
            [e.target.age] : e.target.age,
        }))
    }

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        backgroundColor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    return (
        <Box flex={1} p={2}>
            <Box position="fixed">
            <List
                sx={{ width: '100%', maxWidth: 360}}
                component="nav"
                aria-labelledby="nested-list-subheader"
            >
                <ListItem >
                            <ListItemButton Link to="/">
                                <ListItemIcon>
                                    <Home />
                                </ListItemIcon>
                                <ListItemText primary="Home" />
                            </ListItemButton>
                </ListItem>

                <ListItemButton onClick={handleClick}>
                    <ListItemIcon>
                        <PeopleOutlineIcon />
                    </ListItemIcon>
                    <ListItemText primary="Customers" />
                    {open ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>
                <Collapse in={open} timeout="auto" unmountOnExit>

                    <List component="div" disablePadding>
                        <ListItemButton Link to="/customers" sx={{ pl: 4 }}>
                            <ListItemIcon>
                                <StarBorder />
                            </ListItemIcon>
                            <ListItemText primary="Show Customers" />
                        </ListItemButton>
                    </List>

                    <List component="div" disablePadding>
                        <ListItemButton onClick={handleOpen} sx={{ pl: 4 }} >
                            <ListItemIcon>
                                <StarBorder />
                            </ListItemIcon>
                            <ListItemText primary="Create Customer"/>
                        </ListItemButton>
                    </List>

                </Collapse>


                <ListItemButton onClick={handleClick2}>
                    <ListItemIcon>
                        <AddCardIcon />
                    </ListItemIcon>
                    <ListItemText primary="Accounts" />
                    {open2 ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>

                <Collapse in={open2} timeout="auto" unmountOnExit>

                    <List component="div" disablePadding>
                        <ListItemButton Link to="/accounts" sx={{ pl: 4 }}>
                            <ListItemIcon>
                                <StarBorder />
                            </ListItemIcon>
                            <ListItemText primary="Show Accounts" />
                        </ListItemButton>
                    </List>

                </Collapse>
            </List>


            </Box>

            <Modal
                open={openModal}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h5" component="h5">
                        Customer Information
                    </Typography>

                    <form>
                        <TextField fullWidth name="name" id="name" onChange={handleChange} value={inputs.name} label="name" variant="outlined" sx={{marginTop: 3}}/>
                        <TextField fullWidth name="email" id="email" onChange={handleChange} value={inputs.email} label="email" variant="outlined" sx={{marginTop: 3}} />
                        <TextField fullWidth name="age" id="age" onChange={handleChange} value={inputs.age} label="age" type={"number"} variant="outlined" sx={{marginTop: 3}}/>
                        <div>
                            <Button variant="contained" sx={{margin: 2}} onClick={saveUser}>Enter</Button>
                            <Button variant="outlined" onClick={handleClose} sx={{margin: 2}}>Cancel</Button>
                        </div>

                    </form>
                </Box>
            </Modal>
        </Box>


    );
}

export default Leftbar;
