import {Box} from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import React, {useEffect, useState} from "react";
import CardActions from "@mui/material/CardActions";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import axios from "axios";
import {useLocation} from "react-router";
import {Link} from "react-router-dom";
import { useNavigate } from 'react-router-dom';
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField/TextField";

function Account() {


    const [account, setAccount] = useState([]);
    const [userId, setuserId] = useState();

    let navigate = useNavigate();

    const location = useLocation();

    const [open, setOpen] = useState(true);
    const [open2, setOpen2] = useState(true);


    const [inputs, setInputs] = useState({
        number: "",
        currency: "",
        balance: "",
        customer: {}
    });

    const [deposit, setDeposit] = useState(0);
    const [takeoff, setTakeoff] = useState(0);

    useEffect(()=>{
        refreshAccount();

        // setInputs((prevState) =>({
        //     ...prevState,
        //     name : account.name,
        //     email : account.email,
        //     age : account.age,
        // }))
    }, [userId]);


    const [openModal, setOpenModal] = useState(false);
    const handleOpen = () => setOpenModal(true);
    const handleClose = () => setOpenModal(false);

    const [openModalDeposit, setOpenModalDeposit] = useState(false);
    const handleOpenDeposit = () => setOpenModalDeposit(true);
    const handleCloseDeposit = () => setOpenModalDeposit(false);

    const [openModalTake, setOpenModalTake] = useState(false);
    const handleOpenTake = () => setOpenModalTake(true);
    const handleCloseTake = () => setOpenModalTake(false);

    const handleClick = () => {
        setOpen(!open);
    };
    const handleClick2 = () => {
        setOpen2(!open2);
    };

    const saveAccount = (e) =>{
        e.preventDefault();
        axios.post('http://localhost:9000/accounts', {
            number: inputs.number,
            currency: inputs.currency,
            balance: inputs.balance,
            customer: inputs.customer
        })
            .then(function (response) {

            })
            .catch(function (error) {
                console.log(error);
            });


        setInputs({
            number: "",
            currency: "",
            balance: "",
            customer: {}
        })
        handleClose();
    }

    const updateAccount = (e) =>{

        e.preventDefault();
        axios.put('http://localhost:9000/accounts/' + userId, {
            number: inputs.number,
            currency: inputs.currency,
            balance: inputs.balance,
            customer: inputs.customer
        })
            .then(function (response) {

            })
            .catch(function (error) {
                console.log(error);
            });
        setAccount((prevState)=>({
            ...prevState,
            number: inputs.number,
            currency: inputs.currency,
            balance: inputs.balance,
            customer: inputs.customer
        }))
        handleClose();
    }
    const handleChange = (e) => {
        setInputs((prevState) =>({
            ...prevState,
            [e.target.name] : e.target.value,
            [e.target.email] : e.target.email,
            [e.target.age] : e.target.age,
        }))
    }

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        backgroundColor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    function getId() {
        let pathString = location.pathname.slice(location.pathname.lastIndexOf("/") + 1);
        setuserId(pathString);
        return pathString
    }



    function refreshAccount() {
        console.log("refresh before setId " + userId);
        setuserId(()=>getId());
        console.log("refresh after setId " + userId);
        axios.get(`http://localhost:9000/accounts/${userId}`).then(response => {
            console.log(response.data);
            setAccount(response.data);
            console.log("setAccount(response.data)");
            console.log(account)
        }).catch(error => {
            console.log(error);
        })
    }

    function renewUserState() {
        setInputs((prevState) =>({
            ...prevState,
            number : account.name,
            currency : account.currency,
            balance : account.balance,
            customer: account.customer
        }))
    }

    function deleteaccount(id) {
        console.log("delete" + id);

        axios.delete("http://localhost:9000/accounts/" + id).then(response => {

        }).catch(error => {
            console.log(error);
        })
        navigate('/accounts');

    }
    function renewBalance() {
        axios.put("http://localhost:9000/accounts/add/" + userId,null, { params: { sum: deposit } })
            .then(function (response) {
                refreshAccount();
            })
            .catch(function (error) {
                console.log(error);
            });
        console.log("renewBalance");
        console.log(deposit)
    }

    function takeOffBalance() {
        axios.put("http://localhost:9000/accounts/" + userId,null, { params: { sum: takeoff } })
            .then(function (response) {
                refreshAccount();
            })
            .catch(function (error) {
                console.log(error);
            });
        console.log("renewBalance");
        console.log(deposit)
    }
    const handleChangeBalance = (e) => {
        setDeposit(
            e.target.deposit = e.target.value
        )
        console.log("handleChangeBalance " + deposit);
    }

    const handleChangeTakeOff = (e) => {

        setTakeoff(e.target.takeoof = e.target.value);
        console.log("handleChangeTakeOff " + deposit);
    }


    return (
        <Box sx={{alignItems: "center", justifyContent: "center", minWidth: 600}}>
            <Card sx={{ maxWidth: 945}}>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {account.number}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {account.currency}
                    </Typography>

                    <Typography variant="body2" color="text.secondary">
                        {account.balance}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {/*{account.customer.name}*/}
                        {/*{account.customer.name ? account.customer.name : 'none'}*/}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" onClick={()=> deleteaccount(userId)}>Delete</Button>
                    {/*<Button size="small" onClick={() => {handleOpen(); renewUserState();}}>Update</Button>*/}
                    <Button size="small" onClick={() => {handleOpenDeposit(); renewUserState();}}>Deposit</Button>
                    <Button size="small" onClick={() => {handleOpenTake(); renewUserState();}}>Take off</Button>
                </CardActions>
            </Card>

            <Modal
                open={openModal}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h5" component="h5">
                        account Information
                    </Typography>

                    {/*<form>*/}
                    {/*    <TextField fullWidth name="name" id="name" onChange={handleChange} value={inputs.name} placeholder={account.name} label="name" variant="outlined" sx={{marginTop: 3}}/>*/}
                    {/*    <TextField fullWidth name="email" id="email" onChange={handleChange} value={inputs.email} label="email" variant="outlined" sx={{marginTop: 3}} />*/}
                    {/*    <TextField fullWidth name="age" id="age" onChange={handleChange} value={inputs.age} label="age" type={"number"} variant="outlined" sx={{marginTop: 3}}/>*/}
                    {/*    <div>*/}
                    {/*        <Button variant="contained" sx={{margin: 2}} onClick={updateAccount}>Enter</Button>*/}
                    {/*        <Button variant="outlined" onClick={handleClose} sx={{margin: 2}}>Cancel</Button>*/}
                    {/*    </div>*/}

                    {/*</form>*/}
                </Box>
            </Modal>

            <Modal
                open={openModalDeposit}
                onClose={handleCloseDeposit}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h5" component="h5">
                        Deposit
                    </Typography>

                    <form>
                        <TextField fullWidth name="deposit" id="deposit" onChange={handleChangeBalance} value={deposit} label="deposit" type={"number"} variant="outlined" sx={{marginTop: 3}}/>
                        <div>
                            <Button variant="contained" sx={{margin: 2}} onClick={() => {renewBalance();renewUserState(); handleCloseDeposit();}}>Enter</Button>
                            <Button variant="outlined" onClick={handleCloseDeposit} sx={{margin: 2}}>Cancel</Button>
                        </div>

                    </form>
                </Box>
            </Modal>

            <Modal
                open={openModalTake}
                onClose={handleCloseTake}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h5" component="h5">
                        TakeOff
                    </Typography>

                    <form>
                        <TextField fullWidth name="takeoff" id="takeoff" onChange={handleChangeTakeOff} value={takeoff} label="TakeOff" type={"number"} variant="outlined" sx={{marginTop: 3}}/>
                        <div>
                            <Button variant="contained" sx={{margin: 2}} onClick={() => {takeOffBalance();renewUserState(); handleCloseTake();}}>Enter</Button>
                            <Button variant="outlined" onClick={handleCloseTake} sx={{margin: 2}}>Cancel</Button>
                        </div>

                    </form>
                </Box>
            </Modal>
        </Box>


    );
}

export default Account;
