import {Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import {useEffect, useState} from "react";
import axios from "axios";
import MouseIcon from '@mui/icons-material/Mouse';
import {Link, useNavigate} from "react-router-dom";
import DeleteIcon from '@mui/icons-material/Delete'



function Accounts() {
    const [accounts, setAccounts] = useState([]);

    const [tempRefresh, setTempRefresh] = useState(0)
    let navigate = useNavigate();

    useEffect(()=>{
        refreshAccounts();
    }, [tempRefresh]);

    function refreshAccounts() {
        axios.get("http://localhost:9000/accounts").then(response => {
            setTempRefresh(tempRefresh+1);
            setAccounts(response.data);
        }).catch(error => {
            console.log(error);
        })
    }

    function deleteAccount(id) {
        console.log("delete" + id);

        axios.delete("http://localhost:9000/accounts/" + id).then(response => {

        }).catch(error => {
            console.log(error);
        })
        refreshAccounts();
    }

    return (


        <Box bgcolor={"lightcoral"} flex={4} p={2}>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell >Id</TableCell>
                            <TableCell align="right">Number</TableCell>
                            <TableCell align="right">Currency&nbsp;(g)</TableCell>
                            <TableCell align="right">Balance&nbsp;(g)</TableCell>
                            <TableCell align="right">Customer&nbsp;(g)</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {accounts.map((data, index) => (
                            <TableRow
                                key={data.number}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {data.id}<Link to={"/accounts/" + data.id}><MouseIcon/></Link>
                                </TableCell>
                                <TableCell align="right">{data.number}</TableCell>
                                <TableCell align="right">{data.currency}</TableCell>
                                <TableCell align="right">{data.balance}</TableCell>
                                <TableCell align="right">{data.customer.name}</TableCell>

                                <TableCell align="right"><DeleteIcon cursor="pointer" onClick={() => deleteAccount(data.id)}/></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>


        </Box>

    );
}

export default Accounts;
