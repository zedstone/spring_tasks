import {Box} from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import React, {useEffect, useState} from "react";
import CardActions from "@mui/material/CardActions";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import axios from "axios";
import {useLocation} from "react-router";
import {Link} from "react-router-dom";
import { useNavigate } from 'react-router-dom';
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField/TextField";

function User() {


    const [customer, setCustomer] = useState([]);
    const [userId, setuserId] = useState();
    let navigate = useNavigate();

    const location = useLocation();

    const [open, setOpen] = useState(true);
    const [open2, setOpen2] = useState(true);


    const [inputs, setInputs] = useState({
        name: "",
        email: "",
        age: "",
    });

    useEffect(()=>{
        refreshUser();
        // setInputs((prevState) =>({
        //     ...prevState,
        //     name : customer.name,
        //     email : customer.email,
        //     age : customer.age,
        // }))
    }, [userId]);


    const [openModal, setOpenModal] = useState(false);
    const handleOpen = () => setOpenModal(true);
    const handleClose = () => setOpenModal(false);

    const handleClick = () => {
        setOpen(!open);
    };
    const handleClick2 = () => {
        setOpen2(!open2);
    };

    const saveUser = (e) =>{
        e.preventDefault();
        axios.post('http://localhost:9000/customers', {
            name: inputs.name,
            email: inputs.email,
            age: inputs.age
        })
            .then(function (response) {

            })
            .catch(function (error) {
                console.log(error);
            });


        setInputs({
            name: "",
            email: "",
            age: "",
        })
        handleClose();
    }

    const updateUser = (e) =>{

        e.preventDefault();
        axios.put('http://localhost:9000/customers/' + userId, {
            name: inputs.name,
            email: inputs.email,
            age: inputs.age
        })
            .then(function (response) {

            })
            .catch(function (error) {
                console.log(error);
            });
        setCustomer((prevState)=>({
            ...prevState,
                name :inputs.name,
                email : inputs.email,
                age :inputs.age
        }))
        handleClose();
    }
    const handleChange = (e) => {
        setInputs((prevState) =>({
            ...prevState,
            [e.target.name] : e.target.value,
            [e.target.email] : e.target.email,
            [e.target.age] : e.target.age,
        }))
    }

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        backgroundColor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    function getId() {
        let pathString = location.pathname.slice(location.pathname.lastIndexOf("/") + 1);
        console.log(pathString);
        return pathString
    }




    function refreshUser() {
        setuserId(()=>getId());
        axios.get(`http://localhost:9000/customers/${userId}`).then(response => {
            console.log(response.data);
            setCustomer(response.data);
        }).catch(error => {
            console.log(error);
        })
    }

    function renewUserState() {
        setInputs((prevState) =>({
            ...prevState,
            name : customer.name,
            email : customer.email,
            age : customer.age,
        }))
    }

    function deleteCustomer(id) {
        console.log("delete" + id);

        axios.delete("http://localhost:9000/customers/" + id).then(response => {

        }).catch(error => {
            console.log(error);
        })
        navigate('/customers');

    }


    return (
        <Box sx={{alignItems: "center", justifyContent: "center", minWidth: 600}}>
            <Card sx={{ maxWidth: 945}}>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {customer.name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {customer.email}
                    </Typography>

                    <Typography variant="body2" color="text.secondary">
                        {customer.age}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {customer.accounts}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" onClick={()=> deleteCustomer(userId)}>Delete</Button>
                    <Button size="small" onClick={() => {handleOpen(); renewUserState();}}>Update</Button>
                </CardActions>
            </Card>

            <Modal
                open={openModal}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h5" component="h5">
                        Customer Information
                    </Typography>

                    <form>
                        <TextField fullWidth name="name" id="name" onChange={handleChange} value={inputs.name} placeholder={customer.name} label="name" variant="outlined" sx={{marginTop: 3}}/>
                        <TextField fullWidth name="email" id="email" onChange={handleChange} value={inputs.email} label="email" variant="outlined" sx={{marginTop: 3}} />
                        <TextField fullWidth name="age" id="age" onChange={handleChange} value={inputs.age} label="age" type={"number"} variant="outlined" sx={{marginTop: 3}}/>
                        <div>
                            <Button variant="contained" sx={{margin: 2}} onClick={updateUser}>Enter</Button>
                            <Button variant="outlined" onClick={handleClose} sx={{margin: 2}}>Cancel</Button>
                        </div>

                    </form>
                </Box>
            </Modal>
        </Box>


    );
}

export default User;
