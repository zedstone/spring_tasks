import '../App.css';
import {useEffect, useState} from "react";
import axios from "axios";
import {Box, Button, Stack} from "@mui/material";
import Navbar from "./Navbar";
import Leftbar from "./Leftbar";

import Main from "./Main";

function App() {

  return (
      <Box >
          <Navbar />
          <Stack direction={"row"} spacing={2} justifyContent={"space-between"}>

              <Leftbar />
              <Main />

          </Stack>
      </Box>

  );
}

export default App;



// const [accounts, setaccounts] = useState([]);
// const [accounts, setAccounts] = useState([]);
//
// function goaccounts() {
//     axios.get("http://localhost:9000/accounts").then(response => {
//         console.log(response.data);
//         setaccounts(response.data);
//     }).catch(error => {
//         console.log(error);
//     })
// }
// function goAccounts() {
//     axios.get("http://localhost:9000/accounts").then(response => {
//         console.log(response.data);
//         setAccounts(response.data);
//     }).catch(error => {
//         console.log(error);
//     })
// }
//
// useEffect(() => {
//
// },[]);
//
// <div>
//     <Button variant={"contained"} color={"primary"} size="small" startIcon={<Person />} onClick={goCustomers}>Show Customers</Button>
//     <Button variant={"outlined"} color={"warning"} onClick={goAccounts}>Show Accounts</Button>
// </div>
// <div>
//     {customers.map(customer => {
//         return (
//             <p>{customer.name}</p>
//         )
//     })}
// </div>
// <div>
//     {accounts.map(account => {
//         return (
//             <p>{account.currency}</p>
//         )
//     })}
// </div>