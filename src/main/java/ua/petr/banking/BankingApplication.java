package ua.petr.banking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ua.petr.banking.enums.Currency;
import ua.petr.banking.model.Account;
import ua.petr.banking.model.Customer;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class BankingApplication {


	public static void main(String[] args) {
		SpringApplication.run(BankingApplication.class, args);


	}


}
