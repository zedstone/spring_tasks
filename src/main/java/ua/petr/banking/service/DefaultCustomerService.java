package ua.petr.banking.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.petr.banking.dao.AccountDao;
import ua.petr.banking.dao.CustomerDao;
import ua.petr.banking.model.Customer;
import ua.petr.banking.model.Account;

import ua.petr.banking.enums.Currency;
import java.util.List;
import java.util.Optional;

@Service
public class DefaultCustomerService implements  CustomerService{

    private CustomerDao customerDao;
    private AccountDao accountDao;

    public DefaultCustomerService(CustomerDao customerDao, AccountDao accountDao){
        this.customerDao = customerDao;
        this.accountDao = accountDao;
    }

    @Override
    public Customer save(Customer obj) {
        customerDao.save(obj);
        return obj;
    }

    @Override
    public boolean delete(Customer obj) {
        return customerDao.delete(obj);
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        customerDao.deleteAll(entities);
    }

    @Override
    public void saveAll(List<Customer> entities) {
        customerDao.saveAll(entities);
    }

    @Override
    public List findAll() {
        return customerDao.findAll();
    }

    @Override
    public boolean deleteById(long id) {
        return customerDao.deleteById(id);
    }

    @Override
    public ResponseEntity<?> getOne(long id) {
        return customerDao.getOne(id);
    }

    public void update(Customer customer, long id) {
        customerDao.update(customer, id);
    }

    public void addAccount(Currency currency, long id){
        Optional<Customer> customer = customerDao.customerList.stream().filter(c -> c.getId().equals(id)).findAny();

        System.out.println(customer);
        System.out.println("curency " + currency);

        Account newAccount = new Account(currency,customer.get());
        accountDao.accountList.add(newAccount);
        //customerDao.updateOneAccount(newAccount, id);
        //!One
    }

    public void deleteAccount(Account account, long id) {
        Optional<Customer> customer = customerDao.customerList.stream().filter(c -> c.getId().equals(id)).findAny();
        for (int i = 0; i < customer.get().getAccounts().size(); i++) {
            if(customer.get().getAccounts().get(i).getId() == account.getId()){
                customer.get().getAccounts().remove(i);
            }
        }

        customerDao.save(customer.get());
        accountDao.accountList.remove(account);

    }
}
