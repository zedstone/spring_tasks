package ua.petr.banking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.petr.banking.dao.AccountDao;
import ua.petr.banking.dao.DaoInterface;
import ua.petr.banking.model.Account;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultAccountService implements AccountService{


    private AccountDao accountDao;

    public DefaultAccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public List findAll() {
        return accountDao.findAll();
    }

    @Override
    public Account save(Account obj) {
        return accountDao.save(obj);
    }

    @Override
    public boolean delete(Account obj) {
        return accountDao.delete(obj);
    }

    @Override
    public void deleteAll(List<Account> entities) {
        accountDao.deleteAll(entities);
    }

    @Override
    public void saveAll(List<Account> entities) {
        accountDao.saveAll(entities);
    }



    @Override
    public boolean deleteById(long id) {

        return accountDao.deleteById(id);
    }

    @Override
    public ResponseEntity<?> getOne(long id) {
        return accountDao.getOne(id);
    }

    public boolean plusBalance(Long id, Double sum) {
        if(sum <= 0){
            return false;
        }
        Optional<Account> account = accountDao.accountList.stream().filter(a -> a.getId().equals(id)).findAny();
        if(account.isEmpty()){
            return false;
        }else {
            account.get().setBalance(account.get().getBalance() + sum);
            return true;
        }
    }

    public boolean minusBalance(Long id, Double sum) {
        if(sum >= 0){
            return false;
        }
        Optional<Account> account = accountDao.accountList.stream().filter(a -> a.getId().equals(id)).findAny();
        if(account.isEmpty()){
            return false;
        }else {
            if(account.get().getBalance() + sum < 0){
                return false;
            }
            account.get().setBalance(account.get().getBalance() + sum);
            return true;
        }
    }

    public boolean transfer(Long from, Long to,Double sum) {
        Optional<Account> accountFrom = accountDao.accountList.stream().filter(a -> a.getId().equals(from)).findAny();
        Optional<Account> accountTo = accountDao.accountList.stream().filter(a -> a.getId().equals(to)).findAny();

        if(accountFrom.get().getBalance() < sum || sum <=0){
            return false;
        }else {
            accountFrom.get().setBalance(accountFrom.get().getBalance() - sum);
            accountTo.get().setBalance(accountTo.get().getBalance() + sum);

            return true;
        }
    }
}
