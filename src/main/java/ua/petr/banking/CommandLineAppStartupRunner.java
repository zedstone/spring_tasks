//package ua.petr.banking;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//import ua.petr.banking.service.DefaultCustomerService;
//
//@Component
//public class CommandLineAppStartupRunner implements CommandLineRunner {
//    @Autowired
//    private DefaultCustomerService defaultCustomerService;
//
//    @Override
//    public void run(String...args) throws Exception {
//        defaultCustomerService.save();
//
//    }
//}